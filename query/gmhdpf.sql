select A."CHDRNUM",
	   A."STATCODE",
	   A."MBRNO",
	   A."DPNTNO",
	   CAST(A."DTETRM" AS VARCHAR),
	   A."CLNTNUM",
	   B."CLTTYPE",
	   B."SECUITYNO",
	   B."SALUTL",
	   B."LGIVENAME_SAL",
	   B."LSURNAME",
	   B."LGIVNAME",
	   B."CLTDOB",
	   B."CLTDOD",
	   C."RMBLPHONE",
	   C."RINTERNET",
case 
	when (B."LGIVENAME_SAL" != B."SALUTL") and (LENGTH(C."RMBLPHONE") > 10  or C."RMBLPHONE" !~ '[0-9]\d+$')and (C."RINTERNET" !~  '[A-Za-z@_.]')  then 'SALUTL and RINTERNET and RMBLPHONE incorrect' 
    when (B."LGIVENAME_SAL" != B."SALUTL") and (LENGTH(C."RMBLPHONE") > 10  or C."RMBLPHONE" !~ '[0-9]\d+$') then 'SALUTL and RMBLPHONE incorrect' 
    when (B."LGIVENAME_SAL" != B."SALUTL") and (C."RINTERNET" !~  '[A-Za-z@_.]')then 'SALUTL and RINTERNET incorrect'
    when (B."LGIVENAME_SAL" != B."SALUTL") then 'SALUTL incorrect'
    when (LENGTH(C."RMBLPHONE") > 10  or C."RMBLPHONE" !~ '[0-9]\d+$')and (C."RINTERNET" !~ '[A-Za-z@_.]')  then  'RINTERNET and RMBLPHONE incorrect'
	when (LENGTH(C."RMBLPHONE") > 10  or C."RMBLPHONE" !~ '[0-9]\d+$') then 'RMBLPHONE incorrect' 
	when (C."RINTERNET" !~  '[A-Za-z@_.]') then 'RINTERNET incorrect'
end as FILED
from "Landing_G400_Temp"."GMHDPF" A 
left join "Landing_G400_Temp"."CLNTPF" B on A."CLNTNUM" = B."CLNTNUM"
left join "Landing_G400_Temp"."CLEXPF" C on A."CLNTNUM" = C."CLNTNUM"
