SELECT * from (
SELECT 
A.\"PNO\" ,
A.\"PSTUA\",
A.\"PNAMF\",
A.\"PNAME\",
A.\"PSEX\",
A.\"PDOB\",
A.\"PID\",
A.\"PTEL1\",
A.\"PTEL2\",
A.\"POWNRF\",
A.\"POWNER\",
A.\"POWNID\",
A.\"POWSEX\",
A.\"LPAYDB\",
A.\"PMMBNO\",
A.\"PMOMBNO\",
A.\"PMFXNO\",
A.\"PEMAL\",
A.\"PIEMAL\",
CASE 
  WHEN (A.\"PIEMAL\"!~  '[A-Za-z@_.]' or A.\"PIEMAL\" is  null) AND (A.\"PMMBNO\" !~ '[0-9]d+$' OR LENGTH(A.\"PMMBNO\") > 10)  THEN 'PIEMAL and PMMBNO incorrect'
  WHEN (A.\"PIEMAL\"!~  '[A-Za-z@_.]' or A.\"PIEMAL\" is  null) then 'PIEMAL incorrect'
    --WHEN regexp_replace(A.\"PIEMAL\", '[^ก-๙]', '', 'g') != '' THEN 'PIEMAL incorrect'
  WHEN (A.\"PMMBNO\" !~ '[0-9]d+$' OR LENGTH(A.\"PMMBNO\") > 10) THEN 'PMMBNO incorrect'
END \"FIELD\"
FROM \"Landing_RLS_Temp\".\"LFPAPPL\" A
union
SELECT B.\"PNO\" 
,B.\"PSTU\" as PSTUA 
,B.\"PNAMF\",
B.\"PNAME\",
B.\"PSEX\",
B.\"PDOB\",
B.\"PID\",
B.\"PTEL1\",
B.\"PTEL2\",
B.\"POWNRF\",
B.\"POWNER\",
B.\"POWNID\",
B.\"POWSEX\",
B.\"LPAYDB\",
B.\"PLMBNO\" as PMMBNO ,
B.\"PMOMBNO\",
B.\"PLFXNO\" as PMFXNO ,
B.\"PEMAL\",
B.\"PIEMAL\",
  CASE 
    WHEN (B.\"PIEMAL\"!~  '[A-Za-z@_.]' or B.\"PIEMAL\" is  null) AND (B.\"PLMBNO\" !~ '[0-9]d+$' OR LENGTH(B.\"PLMBNO\") > 10)  THEN 'PIEMAL and PMMBNO incorrect'
    WHEN (B.\"PIEMAL\"!~  '[A-Za-z@_.]' or B.\"PIEMAL\" is  null) then 'PIEMAL incorrect'
    --WHEN regexp_replace(A.\"PIEMAL\", '[^ก-๙]', '', 'g') != '' THEN 'PIEMAL incorrect'
    WHEN (B.\"PLMBNO\" !~ '[0-9]d+$' OR LENGTH(B.\"PLMBNO\") > 10) THEN 'PMMBNO incorrect'
  		END \"FIELD\"
 from \"Landing_RLS_Temp\".\"LFPPML\" B 
)C
where C.\"FIELD\" is not nul