CREATE TABLE "Staging_mockup_data"."PRE_AUTHORIZATION" (
	"UUID" varchar(1200) NULL,
	"PKEY_SRC" varchar(1020) NOT NULL,
	"CLAIM_PKEY_SRC" varchar(1020) NULL,
	"ENTITY_CD" varchar(40) NOT NULL,
	"COMPANY_CD" varchar(40) NOT NULL,
	"PRE_AUTH_NO" varchar(128) NOT NULL,
	"DEL_FLG" varchar(4) NOT NULL,
	"SOURCE_ROWID" varchar(60) NULL,
	"SRC_ROWID" int4 NOT NULL,
	"SRC_SYSTEM_CD" varchar(40) NOT NULL,
	"TRANSACTION_FLAG" varchar(4) NULL,
	"BATCH_ID" varchar(40) NULL,
	"LAST_UPDATE_DATE" timestamp NOT NULL
);