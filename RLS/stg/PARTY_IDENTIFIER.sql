CREATE TABLE "Staging_mockup_data"."PARTY_IDENTIFIER" (
	"UUID" varchar(1200) NULL,
	"PKEY_SRC" varchar(1020) NULL,
	"PARTY_PKEY_SRC" varchar(1020) NULL,
	"ENTITY_CD" varchar(40) NULL,
	"ID_CHECK_DIGIT" varchar(12) NULL,
	"ID_DOC_NO" varchar(400) NULL,
	"ID_DOC_TYPE_CD" varchar(40) NULL,
	"DEL_FLG" varchar(4) NULL,
	"SOURCE_ROWID" varchar(60) NULL,
	"SRC_ROWID" int4 NULL,
	"SRC_SYSTEM_CD" varchar(40) NULL,
	"TRANSACTION_FLAG" varchar(4) NULL,
	"BATCH_ID" varchar(40) NULL,
	"LAST_UPDATE_DATE" timestamp NULL
);