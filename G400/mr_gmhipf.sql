-- "Landing_G400".mr_gmhipf definition

-- Drop table

-- DROP TABLE "Landing_G400".mr_gmhipf;

CREATE TABLE "Landing_G400".mr_gmhipf (
	rrn varchar(30) NOT NULL,
	chdrcoy varchar(30) NULL,
	chdrnum varchar(30) NULL,
	mbrno varchar(30) NULL,
	effdate numeric(24) NULL,
	dtetrm numeric(24) NULL,
	subscoy varchar(3) NULL,
	subsnum varchar(24) NULL,
	occpcode varchar(12) NULL,
	salary numeric(33, 2) NULL,
	dteapp numeric(24) NULL,
	sbstdl varchar(3) NULL,
	termid varchar(12) NULL,
	"user" numeric(18) NULL,
	transaction_date numeric(18) NULL,
	transaction_time numeric(18) NULL,
	tranno numeric(15) NULL,
	fupflg varchar(3) NULL,
	dpntno varchar(6) NULL,
	client varchar(24) NULL,
	personcov varchar(3) NULL,
	mlvlplan varchar(3) NULL,
	clntcoy varchar(3) NULL,
	pccclnt varchar(24) NULL,
	apccclnt varchar(24) NULL,
	earning numeric(33, 2) NULL,
	ctbprcnt numeric(5, 2) NULL,
	ctbamt numeric(30, 2) NULL,
	relashp varchar(12) NULL,
	user_profile varchar(30) NULL,
	job_name varchar(30) NULL,
	datime timestamptz NULL,
	mr_tms timestamptz NULL,
	CONSTRAINT mr_gmhipf_pkey PRIMARY KEY (rrn)
);