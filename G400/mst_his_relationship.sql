REATE TABLE "Landing_G400_Temp".mst_his_relationship (
	chdrnum varchar NOT NULL,
	chdrcoy varchar NOT NULL,
	cownnum varchar NULL,
	subscoy varchar NULL,
	subsnum varchar NULL,
	tranno varchar NULL,
	effective_date varchar NULL,
	expired_date varchar NULL,
	terminated_date varchar NULL,
	update_date varchar NULL
);
COMMENT ON TABLE "Landing_G400_Temp".mst_his_relationship IS 'Capture the history of the relationship between the company and its employees/ employees and our family.';

-- Column comments

COMMENT ON COLUMN "Landing_G400_Temp".mst_his_relationship.chdrnum IS 'Contract Number';
COMMENT ON COLUMN "Landing_G400_Temp".mst_his_relationship.chdrcoy IS 'Contract Header Company';
COMMENT ON COLUMN "Landing_G400_Temp".mst_his_relationship.cownnum IS 'Contract Owner Number';
COMMENT ON COLUMN "Landing_G400_Temp".mst_his_relationship.subscoy IS 'Subsidiary Company';
COMMENT ON COLUMN "Landing_G400_Temp".mst_his_relationship.subsnum IS 'Subsidiary Number';
COMMENT ON COLUMN "Landing_G400_Temp".mst_his_relationship.tranno IS 'Transaction Number';