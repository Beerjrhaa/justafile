-- "Landing_G400".mr_gpsupf definition

-- Drop table

-- DROP TABLE "Landing_G400".mr_gpsupf;

CREATE TABLE "Landing_G400".mr_gpsupf (
	rrn varchar(30) NOT NULL,
	chdrcoy varchar(3) NULL,
	chdrnum varchar(24) NULL,
	subscoy varchar(3) NULL,
	subsnum varchar(24) NULL,
	dteatt numeric(24) NULL,
	dtetrm numeric(24) NULL,
	reasontrm varchar(4) NULL,
	lnbillno numeric(24) NULL,
	labillno numeric(24) NULL,
	lpbillno numeric(24) NULL,
	ptdate numeric(24) NULL,
	ptdateab numeric(24) NULL,
	termid varchar(12) NULL,
	"user" numeric(18) NULL,
	transaction_date numeric(18) NULL,
	transaction_time numeric(18) NULL,
	tranno numeric(15) NULL,
	schdflg varchar(3) NULL,
	mandref varchar(15) NULL,
	user_profile varchar(30) NULL,
	job_name varchar(30) NULL,
	datime timestamptz NULL,
	mr_tms timestamptz NULL,
	sinfdte numeric(212) NULL,
	CONSTRAINT mr_gpsupf_pkey PRIMARY KEY (rrn)
);