CREATE SEQUENCE mst_relationship_code_id_seq;

CREATE TABLE "Landing_G400_Temp".mst_relationship_code (
    id integer NOT NULL DEFAULT nextval('mst_relationship_code_id_seq'),
	party_1 varchar(50) NULL,
	dpntno_1 varchar(50) NULL,
	cltsex_1 varchar(50) NULL,
	party_2 varchar(50) NULL,
	dpntno_2 varchar(50) NULL,
	cltsex_2 varchar(50) NULL,
	code varchar(50) NULL,
	meaning varchar(50) NULL,
	flag varchar(50) NULL,
	create_date varchar(50) NULL,
	update_date varchar(50) NULL
);

ALTER SEQUENCE mst_relationship_code_id_seq
OWNED BY "Landing_G400_Temp".mst_relationship_code.id;



INSERT INTO "Landing_G400_Temp".mst_relationship_code
(party_1, dpntno_1, cltsex_1, party_2, dpntno_2, cltsex_2, code, meaning, flag, create_date, update_date)
select 
party_1, dpntno_1, cltsex_1, party_2, dpntno_2, cltsex_2, code, meaning, flag, create_date, update_date
from  "Master_tables".relationship_code
