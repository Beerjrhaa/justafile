CREATE SEQUENCE mst_salutation_code_id_seq;

CREATE TABLE "Landing_G400_Temp".mst_salutation_code (
    "ID" integer NOT NULL DEFAULT nextval('mst_salutation_code_id_seq'),
    "SALUTL" varchar(40) NULL,
    "TYPE_CD" varchar(2) NULL
);

ALTER SEQUENCE mst_salutation_code_id_seq
OWNED BY "Landing_G400_Temp".mst_salutation_code."ID";



INSERT INTO "Landing_G400_Temp".mst_salutation_code ("SALUTL","TYPE_CD")
select "ABBV" , "Type" from  "Master_tables"."Salutation"