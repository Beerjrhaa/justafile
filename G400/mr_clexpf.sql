-- "Landing_G400".mr_clexpf definition

-- Drop table

-- DROP TABLE "Landing_G400".mr_clexpf;

CREATE TABLE "Landing_G400".mr_clexpf (
	rrn varchar(30) NOT NULL,
	clntpfx varchar(6) NULL,
	clntcoy varchar(3) NULL,
	clntnum varchar(24) NULL,
	rdidtelno varchar(48) NULL,
	rmblphone varchar(48) NULL,
	rpager varchar(48) NULL,
	faxno varchar(48) NULL,
	rinternet varchar(150) NULL,
	rtaxidnum varchar(30) NULL,
	idno varchar(45) NULL,
	rstaflag varchar(6) NULL,
	splindic varchar(6) NULL,
	zspecind varchar(6) NULL,
	oldidno varchar(72) NULL,
	zhubno varchar(45) NULL,
	zoptout varchar(6) NULL,
	znxyrret01 int4 NULL,
	znxyrret02 int4 NULL,
	zidtyp varchar(3) NULL,
	user_profile varchar(30) NULL,
	job_name varchar(30) NULL,
	datime timestamptz NULL,
	mr_tms timestamptz NULL,
	CONSTRAINT mr_clexpf_pkey PRIMARY KEY (rrn)
);